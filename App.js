/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
// } from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';

// import TugasPertama from './src/screen/Tugas1/Intro';
// import Biodata from './src/screen/Tugas2/Biodata';
// import TodoList from './src/screen/Tugas3/TodoList';
// import Context from './src/screen/Tugas4';
// import Splashscreen from './src/screen/Tugas5/routes';
import Homepage from './src/screen/Tugas11/routes';
import { Alert } from 'react-native';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDXAAPBchgCmIsbutaBaDel09_DItQolBw",
  authDomain: "sanbercodelanjutanyoyok.firebaseapp.com",
  databaseURL: "https://sanbercodelanjutanyoyok.firebaseio.com",
  projectId: "sanbercodelanjutanyoyok",
  storageBucket: "sanbercodelanjutanyoyok.appspot.com",
  messagingSenderId: "270381842068",
  appId: "1:270381842068:web:971fb6a83972d5aec43e81",
  measurementId: "G-1JL5SX95E7"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
}

const App: () => React$Node = () => {

  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("9296e8bd-b24a-4499-97da-800c103b18b9", { kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption: 2 });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE
    }, SyncStatus)

    return () => {
      OneSignal.addEventListener('received', onReceived);
      OneSignal.addEventListener('opened', onOpened);
      OneSignal.addEventListener('ids', onIds);
    }
  }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Checking for Update')
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('Downloading Package')
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up to date')
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('Installing Update')
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        console.log("Notification", "Update Installed")
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('Awaiting User')
        break;
      default:
        break;
    }
  }

  const onOpened = (notification) => {
    console.log(notification);
  }

  const onReceived = (openResult) => {
    console.log(openResult);
  }

  const onIds = (device) => {
    console.log(device);
  }

  return (
    // <>
    //   <StatusBar barStyle="dark-content" />
    //   <SafeAreaView>
    //     <ScrollView
    //       contentInsetAdjustmentBehavior="automatic"
    //       style={styles.scrollView}>
    //       <Header />
    //       {global.HermesInternal == null ? null : (
    //         <View style={styles.engine}>
    //           <Text style={styles.footer}>Engine: Hermes</Text>
    //         </View>
    //       )}
    //       <View style={styles.body}>
    //         <View style={styles.sectionContainer}>
    //           <Text style={styles.sectionTitle}>Step One</Text>
    //           <Text style={styles.sectionDescription}>
    //             Edit <Text style={styles.highlight}>App.js</Text> to change this
    //             screen and then come back to see your edits.
    //           </Text>
    //         </View>
    //         <View style={styles.sectionContainer}>
    //           <Text style={styles.sectionTitle}>See Your Changes</Text>
    //           <Text style={styles.sectionDescription}>
    //             <ReloadInstructions />
    //           </Text>
    //         </View>
    //         <View style={styles.sectionContainer}>
    //           <Text style={styles.sectionTitle}>Debug</Text>
    //           <Text style={styles.sectionDescription}>
    //             <DebugInstructions />
    //           </Text>
    //         </View>
    //         <View style={styles.sectionContainer}>
    //           <Text style={styles.sectionTitle}>Learn More</Text>
    //           <Text style={styles.sectionDescription}>
    //             Read the docs to discover what to do next:
    //           </Text>
    //         </View>
    //         <LearnMoreLinks />
    //       </View>
    //     </ScrollView>
    //   </SafeAreaView>
    // </>

    // Tugas 1 RN Lanjutan
    // <TugasPertama />

    // Tugas 2 RN Lanjutan
    // <Biodata />

    // Tugas 3 RN Lanjutan
    // <TodoList />

    // Tugas 4 RN Lanjutan
    // <Context />

    // Tugas 5 RN Lanjutan : Pattern (Perapian Struktur Folder Project)

    // Tugas 5 RN Lanjutan
    // <Splashscreen />

    // Tugas 11 RN Lanjutan
    <Homepage />

  );
};

// const styles = StyleSheet.create({
// scrollView: {
//   backgroundColor: Colors.lighter,
// },
// engine: {
//   position: 'absolute',
//   right: 0,
// },
// body: {
//   backgroundColor: Colors.white,
// },
// sectionContainer: {
//   marginTop: 32,
//   paddingHorizontal: 24,
// },
// sectionTitle: {
//   fontSize: 24,
//   fontWeight: '600',
//   color: Colors.black,
// },
// sectionDescription: {
//   marginTop: 8,
//   fontSize: 18,
//   fontWeight: '400',
//   color: Colors.dark,
// },
// highlight: {
//   fontWeight: '700',
// },
// footer: {
//   color: Colors.dark,
//   fontSize: 12,
//   fontWeight: '600',
//   padding: 4,
//   paddingRight: 12,
//   textAlign: 'right',
// },
// });

export default App;

// Nama project di hadapan publik : project-270381842068
// SHA1 5E:8F:16:06:2E:A3:CD:2C:4A:0D:54:78:76:BA:A6:F3:8C:AB:F6:25 
// SHA1 A2:83:5E:43:C5:10:FE:0D:4B:EB:99:F4:17:E3:50:54:BE:40:D9:60
// pk.eyJ1IjoieW95b2tndW5hbnRvIiwiYSI6ImNrZGZwcnN0eTRsMzQyeHF2bHd5aXl4aTcifQ.ugA4GbaOQL-8wDAe7Z_5nw
// 9296e8bd-b24a-4499-97da-800c103b18b9
// c06397714409f71f99e2a0a863ee6db99decfc5d ( 3z-vxZAKkvhihDUZvDAJOsYdDxK5swI08NKdyk / cyR71k1F_2zCOdWtcemiV0br3GLUpHTYBkzpy )
// code-push release-react Sanbercode android
// code-push deployment list  Sanbercode
// code-push promote Sanbercode Staging Production