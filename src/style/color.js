const color = {
    green: '#18AF02',
    blue: '#3EC6FF',
    darkblue: '#088dc4',
    white: '#ffffff'
}

export default color;