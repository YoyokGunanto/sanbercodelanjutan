import React from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

const InputLogin = ({ title, placeholder, value, onChange }) => {
    return (
        <View style={styles.container}>
            {/* <Text>{title}</Text>
            <TextInput
                style={styles.input}
                placeholder={placeholder}
                placeholderTextColor="gray"
                value={value}
                onChangeText={(value) => { onChange }}
            /> */}
            <Text>{title}</Text>
            <TextInput
                secureTextEntry
                value={value}
                underlineColorAndroid='#C6C6C6'
                placeholder={placeholder}
                onChangeText={onChange}
            />
        </View>
    )
}

export default InputLogin;

const styles = StyleSheet.create({
    container: {
        marginBottom: 7
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: 'purple',
        // borderRadius: 25,
        paddingVertical: 8,
        // paddingHorizontal: 18,
        fontSize: 14,
        color: 'gray',
    }
});