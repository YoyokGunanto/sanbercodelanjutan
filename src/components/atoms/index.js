import InputLogin from './Input/inputLogin';
import InputPassword from './Input/inputPassword';
import ButtonBlue from './Button/ButtonBlue';
import ButtonOrange from './Button/ButtonOrange';

export { InputLogin, InputPassword, ButtonBlue, ButtonOrange };