import React from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

const ButtonBlue = ({ title, onPress }) => {
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

export default ButtonBlue;

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#f02800',
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 10
    },
    text: {
        color: 'white',
    }
});