import React, {
    useState,
} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import MI from 'react-native-vector-icons/MaterialIcons';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

const TodoList = () => {
    const [input, setInput] = useState('');
    const [list, setList] = useState([]);
    const [id, setId] = useState(0);

    const ListItem = (props) => {
        return (
            <View style={styles.boxItem}>
                <View style={{ flex: 0.9 }}>
                    <Text>{props.date}</Text>
                    <Text>{props.value}</Text>
                </View>
                <TouchableOpacity
                    style={styles.delete}
                    onPress={() => deleteList(props.id)}>
                    <MCI name="trash-can-outline" size={25} />
                </TouchableOpacity>
            </View>
        )
    }

    const addList = (input) => {
        let d = new Date();
        setList([...list, {
            id: id,
            date: `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`,
            value: input
        }]);
        setId(id + 1);
        setInput('');
    }

    const deleteList = (id) => {
        setList((list) => {
            return list.filter((list) => list.id !== id);
        });
    }

    return (
        <View style={styles.container}>
            <View style={styles.wrappingInput}>
                <Text>Masukkan Todolist</Text>
                <View style={styles.boxInput}>
                    <TextInput
                        style={styles.input}
                        value={input}
                        onChangeText={(val) => setInput(val)}
                        placeholder='Input here'
                    />
                    <TouchableOpacity
                        style={styles.buttonAdd}
                        onPress={() => addList(input)}
                    >
                        <MI name="add" size={25} />
                    </TouchableOpacity>
                </View>

            </View>
            <FlatList
                data={list}
                style={styles.dataList}
                renderItem={({ item }) => (
                    <ListItem id={item.id} date={item.date} value={item.value} />
                )}
                keyExtractor={(item) => item.id.toString()}
                ItemSeparatorComponent={() => <View style={{ height: 10 }} />}
            />
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    wrappingInput: {
        padding: 10,
    },
    boxInput: {
        flexDirection: 'row',
        marginTop: 10,
    },
    input: {
        flex: 1,
        paddingHorizontal: 5,
        borderWidth: 1,
    },
    buttonAdd: {
        width: 50,
        height: 50,
        marginLeft: 5,
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dataList: {
        flex: 1,
        padding: 10,
    },
    boxItem: {
        flex: 1,
        padding: 15,
        borderRadius: 5,
        borderWidth: 5,
        borderColor: '#BDC3C7',
        flexDirection: 'row',
    },
    delete: {
        flex: 0.1,
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default TodoList
