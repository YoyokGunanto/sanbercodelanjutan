import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    Image,
    TouchableOpacity,
} from 'react-native'
import Axios from 'axios';
import api from '../../api';
import Asyncstorage from '@react-native-community/async-storage';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';

import { ButtonBlue } from '../../components';

let Width = Dimensions.get('window').width
let Height = Dimensions.get('window').height

const Biodata = ({ navigation }) => {

    const [userInfo, setUserInfo] = useState(null);

    useEffect(() => {
        async function getToken() {
            try {
                const token = await Asyncstorage.getItem('token');
                return getVenue(token);
                console.log(token);
            } catch (err) {
                console.log(err);
            }
        }
        getToken();
        getCurrentUser();
    }, [])

    const getVenue = (token) => {
        Axios.get(`${api}/venues`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.log('Gagal', err);
            })
    }

    const onLogoutPress = async () => {
        try {
            await GoogleSignin.revokeAccess();// Meghilangkan akses login
            await GoogleSignin.signOut();// Logout
            await Asyncstorage.removeItem('token');
            // auth().signOut(); // Logout with EmailAndPassword
            setUserInfo('');
            navigation.replace('Login');
        } catch (err) {
            console.log(err);
        }
    }

    const getCurrentUser = async () => {
        try {
            const userInfo = await GoogleSignin.signInSilently();
            setUserInfo(userInfo);
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                // user has not signed in yet
                alert('user has not signed in yet');
            } else {
                // some other error
                alert('some other error');
            }
        }
    }


    return (
        <View style={styles.container}>
            <View style={styles.containerProfil}>
                {/* <Profile /> */}
                <View style={styles.wrappingProfile}>
                    {/* <Image source={{ uri: 'https://gitlab.com/YoyokGunanto/sanbercodeimrn0620/-/raw/master/SanberApp/Tugas/Tugas13/images/me.jpg' }} style={styles.profilImage} /> */}
                    {/* <Image source={require('../../assets/images/me.jpg')} style={styles.profilImage} /> */}
                    <Image source={{ uri: userInfo && userInfo.user && userInfo.user.photo }} style={styles.profilImage} />
                    <Text style={styles.profilText}>{userInfo && userInfo.user && userInfo.user.name}</Text>
                </View>
            </View>
            <View style={styles.containerBiodata}>
                {/* <BiodataDetail onPress={() => onLogoutPress()} /> */}
                <View style={styles.wrappingBiodata}>
                    <View style={styles.boxBiodata}>
                        <View style={styles.boxTitle}><Text>Tanggal lahir</Text></View>
                        <View style={styles.boxValue}><Text>20 Juli 1994</Text></View>
                    </View>
                    <View style={styles.boxBiodata}>
                        <View style={styles.boxTitle}><Text>Jenis kelamin</Text></View>
                        <View style={styles.boxValue}><Text>Laki - laki</Text></View>
                    </View>
                    <View style={styles.boxBiodata}>
                        <View style={styles.boxTitle}><Text>Hobi</Text></View>
                        <View style={styles.boxValue}><Text>Ngoding</Text></View>
                    </View>
                    <View style={styles.boxBiodata}>
                        <View style={styles.boxTitle}><Text>No. Telp</Text></View>
                        <View style={styles.boxValue}><Text>082338109646</Text></View>
                    </View>
                    <View style={styles.boxBiodata}>
                        <View style={styles.boxTitle}><Text>Email</Text></View>
                        <View style={styles.boxValue}><Text>{userInfo && userInfo.user && userInfo.user.email}</Text></View>
                    </View>
                    <View style={styles.btnContainner}>
                        {/* <TouchableOpacity style={styles.logout} onPress={onPress}>
                            <Text style={{ color: '#FFFFFF' }}>LOGOUT</Text>
                        </TouchableOpacity> */}
                        <ButtonBlue title="LOGOUT" onPress={() => { onLogoutPress() }} />
                    </View>
                </View>
            </View>
        </View>
    )
}

// const Profile = () => {
//     return (
//         <View style={styles.wrappingProfile}>
//             {/* <Image source={{ uri: 'https://gitlab.com/YoyokGunanto/sanbercodeimrn0620/-/raw/master/SanberApp/Tugas/Tugas13/images/me.jpg' }} style={styles.profilImage} /> */}
//             <Image source={require('../../assets/images/me.jpg')} style={styles.profilImage} />
//             <Text style={styles.profilText}>Yoyok Gunanto</Text>
//         </View>
//     );
// }

// const BiodataDetail = ({ onPress }) => {

//     const [userInfo, setUserInfo] = useState(null);

//     useEffect(() => {
//         async function getToken() {
//             try {
//                 const token = await Asyncstorage.getItem('token');
//                 return getVenue(token);
//                 console.log(token);
//             } catch (err) {
//                 console.log(err);
//             }
//         }
//         getToken();
//         getCurrentUser();
//     }, [])

//     const getVenue = (token) => {
//         Axios.get(`${api}/venues`, {
//             timeout: 20000,
//             headers: {
//                 'Authorization': 'Bearer' + token
//             }
//         })
//             .then((res) => {
//                 console.log(res);
//             })
//             .catch((err) => {
//                 console.log('Gagal', err);
//             })
//     }

//     const getCurrentUser = async () => {
//         const userInfo = await GoogleSignin.signInSilently();
//         setUserInfo(userInfo);
//     }

//     return (
//         <View style={styles.wrappingBiodata}>
//             <View style={styles.boxBiodata}>
//                 <View style={styles.boxTitle}><Text>Tanggal lahir</Text></View>
//                 <View style={styles.boxValue}><Text>20 Juli 1994</Text></View>
//             </View>
//             <View style={styles.boxBiodata}>
//                 <View style={styles.boxTitle}><Text>Jenis kelamin</Text></View>
//                 <View style={styles.boxValue}><Text>Laki - laki</Text></View>
//             </View>
//             <View style={styles.boxBiodata}>
//                 <View style={styles.boxTitle}><Text>Hobi</Text></View>
//                 <View style={styles.boxValue}><Text>Ngoding</Text></View>
//             </View>
//             <View style={styles.boxBiodata}>
//                 <View style={styles.boxTitle}><Text>No. Telp</Text></View>
//                 <View style={styles.boxValue}><Text>082338109646</Text></View>
//             </View>
//             <View style={styles.boxBiodata}>
//                 <View style={styles.boxTitle}><Text>Email</Text></View>
//                 <View style={styles.boxValue}><Text>gunantoyoyok@gmail.com</Text></View>
//             </View>
//             <View style={styles.btnContainner}>
//                 {/* <TouchableOpacity style={styles.logout} onPress={onPress}>
//                     <Text style={{ color: '#FFFFFF' }}>LOGOUT</Text>
//                 </TouchableOpacity> */}
//                 <ButtonBlue title="LOGOUT" onPress={onPress} />
//             </View>
//         </View>
//     );
// }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: Width
    },
    containerProfil: {
        width: Width,
        height: Height * 0.35,
        backgroundColor: '#3EC6FF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    wrappingProfile: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    profilImage: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
    },
    profilText: {
        fontSize: 18,
        marginTop: 10,
        color: 'white',
        fontWeight: 'bold',
    },
    containerBiodata: {
        width: Width,
        height: Height * 0.65,
        backgroundColor: 'white',
    },
    wrappingBiodata: {
        flex: 0.5,
        height: Height * 0.4,
        padding: 10,
        marginTop: -30,
        marginHorizontal: 30,
        elevation: 5,
        borderRadius: 8,
        backgroundColor: 'white',
    },
    boxBiodata: {
        flex: 1,
        flexDirection: 'row',
    },
    boxTitle: {
        flex: 2,
        marginTop: 10,
        marginHorizontal: 10,
        alignItems: 'flex-start',
    },
    boxValue: {
        flex: 3,
        marginTop: 10,
        marginHorizontal: 10,
        alignItems: 'flex-end',
    },
    btnContainner: {
        marginTop: 20,
    },
    logout: {
        marginTop: 20,
        backgroundColor: '#3EC6FF',
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 10
    }
})

export default Biodata;
