import React, {
    useState, createContext
} from 'react';
import {
    View,
    Text,
} from 'react-native';
import TodoList from '../Tugas4/TodoList';

export const RootContext = createContext(); // Provider

const Context = () => {

    const [input, setInput] = useState('');
    const [todos, setTodos] = useState([]);
    const [id, setId] = useState(0);

    handleChangeInput = (value) => {
        setInput(value);
    }

    addTodo = () => {
        const day = new Date().getDate();
        const month = new Date().getMonth();
        const year = new Date().getFullYear();

        const today = `${day}/${month}/${year}`;
        setTodos([...todos, {
            id: id,
            title: input,
            date: today
        }]);
        setId(id + 1);
        setInput('');
    }

    deleteTodo = (id) => {
        setTodos((todos) => {
            return todos.filter((todos) => todos.id !== id);
        });
    }

    return (
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            addTodo,
            deleteTodo
        }}>
            <TodoList />
        </RootContext.Provider>
    )
}


export default Context
