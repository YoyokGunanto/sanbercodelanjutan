import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15,
        backgroundColor: "#ffffff",
    },
    slide: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ffffff",
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#190ca6',
    },
    image: {
        marginVertical: 35
    },
    text: {
        fontSize: 14.5,
        textAlign: "center",
        color: '#949494',
    },
    buttonCircle: {
        flex: 1,
        width: 50,
        height: 50,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 50 / 2,
        backgroundColor: '#190ca6',
    },
    containnerLine: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
    },
    lineStyle: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#C6C6C6',
        marginVertical: 30
    },
    containerLoad: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
})