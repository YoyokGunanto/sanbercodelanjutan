import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator
} from 'react-native';
import styles from './style';
import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import api from '../../api';
import auth from '@react-native-firebase/auth';

import { InputLogin, InputPassword, ButtonBlue, ButtonOrange } from '../../components';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';

import TouchID from 'react-native-touch-id';

import { CommontActions } from '@react-navigation/native';

const config = {
    title: 'Authentication Required', // Android
    imageColor: '#191970', // Android
    imageErrorColor: 'red', // Android
    sensorDescription: 'Touch sensor', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Android
}

function Login({ navigation }) {


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const [token, setToken] = useState('');

    const saveToken = async (token) => {
        try {
            await Asyncstorage.setItem('token', token)
        } catch (err) {
            console.log(err)
        }
    }

    const onLoginPress = () => {
        if (email != '' && password != '') {
            setIsLoading(true);
            let data = {
                email: email,
                password: password
            }
            Axios.post(`${api}/login`, data, {
                timeout: 20000
            }).then((res) => {
                saveToken(res.data.token)
                // navigation.navigate('Profile')

                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Home' }]
                })
            }).catch((err) => {
                setIsLoading(false);
                alert('Login Failed');
                setEmail('');
                setPassword('');
            })
        } else {
            alert('Masukkan username dan password');
        }
    }

    useEffect(() => {
        setInterval(() => {
            setIsLoading(false);
        }, 10)
        configurGoogleSignin();
        setToken('');
    }, [token]);

    const configurGoogleSignin = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '270381842068-3bmgnc7bkdaobo8rsudl4enfm11s4b2q.apps.googleusercontent.com'
        });
    }

    const signinWithGoogle = async () => {
        setIsLoading(true)
        try {
            setIsLoading(false)
            // Tambahan
            await GoogleSignin.hasPlayServices();

            const { idToken } = await GoogleSignin.signIn()
            // .then((idToken) => {
            setToken(idToken);

            const credential = auth.GoogleAuthProvider.credential(idToken);
            auth().signInWithCredential(credential);

            navigation.navigate('Profile');
            // })
            // .catch((err) => {
            //     console.log('WRONG SIGNIN', err);
            // })
            // .done();

        } catch (error) {
            setIsLoading(false)
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                alert('user cancelled the login flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
                alert('operation (e.g. sign in) is in progress already');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                alert('play services not available or outdated');
            } else {
                // some other error happened
                alert('some other error happened');
            }
        }
    }

    const signInWithFingerprint = () => {
        TouchID.authenticate('', config)
            .then(success => {
                // alert('Authentication Success');
                setIsLoading(true);
            })
            .catch(error => {
                alert('Authentication Error');
            })
    }

    // const onLoginPress = () => {
    //     if (email != '' && password != '') {
    //         setIsLoading(true);
    //         return auth().signInWithEmailAndPassword(email, password)
    //             .then((res) => {
    //                 // navigation.navigate('Home')

    //                 navigation.reset({
    //                     index: 0,
    //                     routes: [{ name: 'Home' }]
    //                 })

    //                 // navigation.dispatch(
    //                 //     CommontActions.reset({
    //                 //         index: 0,
    //                 //         routes: [{ name: 'Home' }]
    //                 //     })
    //                 // )
    //             })
    //             .catch((error) => {
    //                 console.log(error)
    //                 alert('Username atau password salah');
    //             })
    //     } else {
    //         alert('Masukkan username dan password');
    //     }
    // }

    const registration = () => {
        navigation.navigate('Registration');
    }

    if (isLoading) {
        return (
            <View style={[styles.containerLoad, styles.horizontal]}>
                <ActivityIndicator size="large" color="#00ff00" style={styles.loading} />
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Image source={require('../../assets/images/logo.jpg')} />
                </View>
                <View style={styles.content}>
                    <View style={styles.formContainner}>
                        {/* <Text>Username</Text>
                    <TextInput
                    value={email}
                        underlineColorAndroid='#C6C6C6'
                        placeholder='Username or Email'
                        onChangeText={(email) => setEmail(email)}
                    /> */}
                        <InputLogin title="Username" placeholder="Username or Email" value={email} onChange={(email) => setEmail(email)} />
                        {/* <Text>Password</Text>
                    <TextInput
                        secureTextEntry
                        value={password}
                        underlineColorAndroid='#C6C6C6'
                        placeholder='Password'
                    onChangeText={(password) => setPassword(password)} 
                    />*/}
                        <InputPassword title="Username" placeholder="Username or Email" value={password} onChange={(password) => setPassword(password)} />
                    </View>
                    <View style={styles.btnContainner}>
                        {/* <View>
                        <Button
                            color="#3EC6FF"
                            title="LOGIN"
                            onPress={() => onLoginPress()}
                        />
                    </View> */}
                        <ButtonBlue title="LOGIN (Codepush)" onPress={() => onLoginPress()} />
                        <View style={styles.containnerLine}>
                            <View style={styles.lineStyle} />
                            <Text style={{ paddingHorizontal: 8 }}>OR</Text>
                            <View style={styles.lineStyle} />
                        </View>
                        {/* <ButtonOrange title="LOGIN WITH GOOGLE" onPress={() => alert('LOGIN WITH GOOGLE')} /> */}
                        <GoogleSigninButton
                            onPress={() => signinWithGoogle()}
                            style={{ width: '100%', height: 48, }}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Light}
                        />
                        <Button
                            color="#191970"
                            title="SIGN IN WITH FINGERPRINT"
                            onPress={() => signInWithFingerprint()}
                        />
                    </View>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "flex-end", marginTop: 10 }}>
                    <Text>Belum mempunyai aku ? </Text>
                    <TouchableOpacity onPress={() => registration()}>
                        <Text style={{ color: '#191970' }}>Buat Akun</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

export default Login;