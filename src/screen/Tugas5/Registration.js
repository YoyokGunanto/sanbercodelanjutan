import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    StatusBar,
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView,
    Modal,
} from 'react-native';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Feat from 'react-native-vector-icons/Feather';

import { RNCamera } from 'react-native-camera';
import storage from '@react-native-firebase/storage';

import { InputLogin, InputPassword, ButtonBlue } from '../../components';

const Registration = ({ navigation }) => {

    const [form, setForm] = useState({
        name: '',
        email: '',
        password: '',
    });
    const [isVisible, setIsVisible] = useState(false);
    const [type, setType] = useState('back');
    const [photo, setPhoto] = useState(null);

    const toggleCamera = () => {
        setType(type === 'back' ? 'front' : 'back');
    }

    const takePicture = async () => {
        const options = { quality: 0.5, base64: true }
        if (camera) {
            const data = await camera.takePictureAsync(options);
            console.log(data);
            setPhoto(data);
            setIsVisible(false);
        }
    }

    const uploadOmage = (uri) => {
        const sessionId = new Date().getTime();
        return storage()
            .ref(`images/${sessionId}`)
            .putFile(uri)
            .then((response) => {
                alert('Success Upload')
            })
            .catch((error) => {
                alert(error)
            })
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        ref={ref => {
                            camera = ref;
                        }}
                        type={type}
                    >
                        <View style={styles.btnFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
                                <MCI name="rotate-3d-variant" size={15} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.round} />
                        <View style={styles.rectangle} />
                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                                <Feat name="camera" size={35} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        );
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.containerProfil}>
                    <View style={styles.wrappingProfile}>
                        <Image source={photo === null ? require('../../assets/images/me.jpg') : { uri: photo.uri }} style={styles.profilImage} />
                        <TouchableOpacity onPress={() => setIsVisible(true)}>
                            <Text style={styles.profilText}>Change picture</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.containerBiodata}>
                    <View style={styles.wrappingBiodata}>
                        <View style={styles.boxBiodata}>
                            <InputLogin title="Name" placeholder="Name" value={form.form} onChange={(value) => setForm(value)} />
                        </View>
                        <View style={styles.boxBiodata}>
                            <InputLogin title="Email" placeholder="Email" value={form.email} onChange={(value) => setForm(value)} />
                        </View>
                        <View style={styles.boxBiodata}>
                            <InputPassword title="Password" placeholder="Password" value={form.password} onChange={(value) => setForm(value)} />
                        </View>
                        <View style={styles.btnContainner}>
                            <ButtonBlue title="REGISTRE" onPress={() => uploadOmage(photo.uri)} />
                        </View>
                    </View>
                </View>
            </ScrollView>
            {renderCamera()}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerProfil: {
        paddingVertical: 50,
        backgroundColor: '#3EC6FF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    wrappingProfile: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    profilImage: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
    },
    profilText: {
        fontSize: 18,
        marginTop: 10,
        color: 'white',
        fontWeight: 'bold',
    },
    containerBiodata: {
        backgroundColor: 'white',
    },
    wrappingBiodata: {
        padding: 10,
        marginTop: -30,
        marginHorizontal: 30,
        elevation: 5,
        borderRadius: 8,
        backgroundColor: 'white',
    },
    boxBiodata: {
        marginBottom: 10,
        flexDirection: 'column',
    },
    boxTitle: {
        marginTop: 10,
        marginHorizontal: 10,
    },
    boxValue: {
        marginTop: 10,
        marginHorizontal: 10,
    },
    btnContainner: {
        marginTop: 10,
    },
    logout: {
        marginTop: 20,
        backgroundColor: '#3EC6FF',
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 10
    },
    btnFlipContainer: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },
    btnFlip: {

    },
    round: {
        width: 120,
        height: 150,
        borderRadius: 120 / 2,
        borderWidth: 1,
        marginTop: 80,
        borderColor: 'white',
        alignSelf: "center"
    },
    rectangle: {
        width: 100,
        height: 80,
        borderWidth: 1,
        marginTop: 50,
        borderColor: 'white',
        alignSelf: "center"
    },
    btnTakeContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignSelf: "center"
    },
    btnTake: {
        width: 80,
        height: 80,
        borderRadius: 80 / 2,
        marginBottom: 20,
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: "center",
    }
})

export default Registration;
