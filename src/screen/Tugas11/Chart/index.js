import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, processColor } from 'react-native';
import { BarChart } from 'react-native-charts-wrapper';

import colors from '../../../style/color';

const ChartPage = ({ navigation }) => {

    const data = [
        { y: [100, 40], marker: ["React Native Dasar", "React Native Lanjutan"] },
        { y: [80, 60], marker: ["React Native Dasar", "React Native Lanjutan"] },
        { y: [40, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
        { y: [78, 45], marker: ["React Native Dasar", "React Native Lanjutan"] },
        { y: [67, 87], marker: ["React Native Dasar", "React Native Lanjutan"] },
        { y: [98, 32], marker: ["React Native Dasar", "React Native Lanjutan"] },
        { y: [150, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
    ];
    // [{ y: 100, marker: 'Contoh detail marker' }, { y: 105 }, { y: 102 }, { y: 110 }, { y: 114 }, { y: 109 }, { y: 105 }, { y: 100 }]

    // const [dataChart, setDataChart] = useState([]);

    // const showMasking = (data) => {
    let newData = [];
    data.forEach((element, index) => {
        // newList = element.marker[0] + ' - ' + element.marker[1];
        newData.push({ y: element.y, marker: [`${element.marker[0]}\n${element.y[0]}`, `${element.marker[1]}\n${element.y[1]}`] });
    });
    // alert(newList.marker);
    // setDataChart(newList);
    // console.log(dataChart);
    // return newList;
    // }

    // useEffect(() => {
    //     // console.log(data.length);
    //     // alert(data[0].y[0]);
    //     // console.log(data);
    //     setTimeout(() => {
    //         showMasking(data);
    //     })
    // async function getData(data) {
    // showMasking(data);
    // }
    // getData(data);
    // }, [dataChart]);

    const [legend, setLegend] = useState({
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })

    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: newData,
                label: '',
                config: {
                    colors: [processColor(colors.blue), processColor(colors.darkblue)],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
    })

    const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            enabled: false
        }
    })

    return (
        <View style={{ flex: 1, backgroundColor: colors.white }}>
            <BarChart
                style={{ flex: 1 }}
                data={chart.data}
                yAxis={yAxis}
                xAxis={xAxis}
                pinchZoom={false}
                doubleTapToZoomEnabled={false}
                chartDescription={{ text: '' }}
                legend={legend}
                animation={{ durationX: 1000 }}
                drawBarShadow={false}
                drawValueAboveBar={true}
                drawHighlightArrow={true}
                marker={{
                    enabled: true,
                    markerColor: 'gray',
                    textColor: 'white',
                    textSize: 14
                }}
            />
            {/* gridBackgroundColor={'#ffffff'} */}
        </View>
    );

}

export default ChartPage;

const styles = StyleSheet.create({

});