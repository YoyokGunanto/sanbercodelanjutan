import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Ent from 'react-native-vector-icons/Entypo';
import color from '../../style/color';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import SplashScreen from '../Tugas5/Splashscreen';
import Intro from '../Tugas5/Intro';
import Home from './Home';
import Maps from './Maps';
import Profile from './Profile';
import Chart from './Chart';
import Chat from './Chat';
import Login from '../Tugas5/Login';

import Asyncstorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const TabsScreen = () => {
    return (
        <Tabs.Navigator
            initialRouteName="Home"
            tabBarOptions={{
                activeTintColor: color.darkblue,
            }}
        >
            <Tabs.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, size }) => (
                        <MCI name="home" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="Maps"
                component={Maps}
                options={{
                    tabBarLabel: 'Maps',
                    tabBarIcon: ({ color, size }) => (
                        <Ent name="location" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="Chat"
                component={Chat}
                options={{
                    tabBarLabel: 'Chat',
                    tabBarIcon: ({ color, size }) => (
                        <Ent name="chat" color={color} size={size} />
                    ),
                }}
            />
            <Tabs.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                        <MCI name="account" color={color} size={size} />
                    ),
                }}
            />
        </Tabs.Navigator>
    );
}

const MainNavigation = (props) => (
    <Stack.Navigator initialRouteName={props.token != null && 'Home'}>
        {props.intro == null && <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />}
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={TabsScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Chart" component={Chart} options={{ headerShown: true, title: 'React Native' }} />
    </Stack.Navigator>
)

const RoutesPage = () => {

    const [isLoading, setIsLoading] = useState(true);
    const [intro, setIntro] = useState(null);
    const [token, setToken] = useState(null);

    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 2000)

        async function getStatus() {
            try {
                const skipped = await Asyncstorage.getItem('skipped');
                if (skipped !== null) {
                    // console.log(skipped);
                    setIntro(skipped);
                } else {
                    setIntro(null);
                }
            } catch (err) {
                console.log(err)
            }
        }

        async function getToken() {
            try {
                const token = await Asyncstorage.getItem('token');
                if (token !== null) {
                    setToken(token);
                } else {
                    setToken(null);
                }
            } catch (err) {
                console.log(err)
            }
        }
        getStatus();
        getToken();
    }, [])

    if (isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation intro={intro} token={token} />
        </NavigationContainer>
    );

}

export default RoutesPage;

const styles = StyleSheet.create({

});