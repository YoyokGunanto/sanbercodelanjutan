import React, { useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken('pk.eyJ1IjoieW95b2tndW5hbnRvIiwiYSI6ImNrZGZwcnN0eTRsMzQyeHF2bHd5aXl4aTcifQ.ugA4GbaOQL-8wDAe7Z_5nw');
// 108.419641, -7.337977
// 110.327168, -7.695752 Sleman

const MapsPage = () => {

    const coordinates = [
        [107.598827, -6.896191],
        [107.596198, -6.899688],
        [107.618767, -6.902226],
        [107.621095, -6.898690],
        [107.615698, -6.896741],
        [107.613544, -6.897713],
        [107.613697, -6.893795],
        [107.610714, -6.891356],
        [107.605468, -6.893124],
        [107.609180, -6.898013]
    ];

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions();
            } catch (error) {
                console.log(error);
            }
        }
        getLocation();
    }, []);

    const reanderPointAnnotation = (data) => {
        return data.map((items, index) => {
            let [longitude, latitude] = items;
            return (
                <MapboxGL.PointAnnotation
                    id={`pointAnnotation-${index}`}
                    coordinate={items}
                    key={index}
                >
                    <MapboxGL.Callout
                        title={`Longitude: ${longitude}\nLatitude: ${latitude}`}
                    />
                </MapboxGL.PointAnnotation>
            )
        })

    }

    return (
        <View style={{ flex: 1 }}>
            <MapboxGL.MapView
                style={{ flex: 1 }}
            >
                <MapboxGL.UserLocation
                    visible={true}
                />
                <MapboxGL.Camera
                    followUserLocation={true}
                />

                {reanderPointAnnotation(coordinates)}

                {/* <MapboxGL.PointAnnotation
                    id="pointAnnotation"
                    coordinate={[108.419641, -7.337977]}
                >
                    <MapboxGL.Callout
                        title="Ini adalah callout dari sebuah marker"
                    />
                </MapboxGL.PointAnnotation> */}

            </MapboxGL.MapView>
        </View>
    );

}

export default MapsPage;