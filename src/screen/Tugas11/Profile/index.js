import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator
} from 'react-native';

import Asyncstorage from '@react-native-community/async-storage';

import Ant from 'react-native-vector-icons/AntDesign';

const ProfilePage = ({ navigation }) => {

    const [userInfo, setUserInfo] = useState(null);
    const [token, setToken] = useState('');

    useEffect(() => {
        async function getToken() {
            try {
                const token = await Asyncstorage.getItem('token');
                setToken(token);
            } catch (err) {
                console.log(err);
            }
        }
        getToken();
    }, []);

    const logoutPress = async () => {
        try {
            if (token) {
                await Asyncstorage.removeItem('token');
            } else {
                // await GoogleSignin.revokeAccess();// Meghilangkan akses login
                // await GoogleSignin.signOut();// Logout
            }
            setUserInfo(null);
            navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }],
                params: {
                    tes: 'cara mengirim params'
                }
            });
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#FFFFFF' }}>
            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 10, borderRadius: 50, borderColor: 'red', borderWidth: 2, flexDirection: 'row', justifyContent: "center", alignItems: "center" }} onPress={() => logoutPress()}>
                <Ant name="logout" size={18} color="red" />
                <Text style={{ color: 'red', fontWeight: 'bold' }}> LOGOUT</Text>
            </TouchableOpacity>
        </View>
    );

}

export default ProfilePage;

const styles = StyleSheet.create({
});