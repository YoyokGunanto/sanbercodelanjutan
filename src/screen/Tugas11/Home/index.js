import React from 'react';
import { View, Text, StyleSheet, StatusBar, Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';

import color from '../../../style/color';

import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Ion from 'react-native-vector-icons/Ionicons';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const HomePage = ({ navigation }) => {

    return (
        <View style={styles.containner}>
            <StatusBar backgroundColor={color.white} barStyle="dark-content" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.cardContainner}>
                    <View style={styles.cardHeader}>
                        <Text style={styles.titleCard}>Kelas</Text>
                    </View>
                    <View style={styles.cardBody}>
                        <TouchableOpacity style={styles.itemKelas} onPress={() => navigation.navigate('Chart', { name: 'Yoyok Gunanto' })}>
                            <Ion name="logo-react" style={styles.iconItemKelas} />
                            <Text style={{ color: color.white }}>React Native</Text>
                        </TouchableOpacity>
                        <View style={styles.itemKelas}>
                            <Ion name="logo-python" style={styles.iconItemKelas} />
                            <Text style={{ color: color.white }}>Data Science</Text>
                        </View>
                        <View style={styles.itemKelas}>
                            <Ion name="logo-react" style={styles.iconItemKelas} />
                            <Text style={{ color: color.white }}>React JS</Text>
                        </View>
                        <View style={styles.itemKelas}>
                            <Ion name="logo-laravel" style={styles.iconItemKelas} />
                            <Text style={{ color: color.white }}>Laravel</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.cardContainner}>
                    <View style={styles.cardHeader}>
                        <Text style={styles.titleCard}>Kelas</Text>
                    </View>
                    <View style={styles.cardBody}>
                        <View style={styles.itemKelas}>
                            <Ion name="logo-wordpress" style={styles.iconItemKelas} />
                            <Text style={{ color: color.white }}>Wordpress</Text>
                        </View>
                        <View style={styles.itemKelas}>
                            <Image source={require('../../../assets/icon/website-design.png')} style={styles.imageItemKelas} />
                            <Text style={{ color: color.white }}>Design Grafis</Text>
                        </View>
                        <View style={styles.itemKelas}>
                            <MCI name="server" style={styles.iconItemKelas} />
                            <Text style={{ color: color.white }}>Web Server</Text>
                        </View>
                        <View style={styles.itemKelas}>
                            <Image source={require('../../../assets/icon/ux.png')} style={styles.imageItemKelas} />
                            <Text style={{ color: color.white }}>UI/UX Design</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.cardContainner}>
                    <View style={styles.cardHeader}>
                        <Text style={styles.titleCard}>Summary</Text>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>React Native</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>20 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>Data Science</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>30 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>React JS</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>66 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>Laravel</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>60 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>Wordpress</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>60 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>Design Grafis</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>60 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>Web Server</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>60 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardBodySummary}>
                        <Text style={styles.titleItemSammery}>UI/UX Design</Text>
                        <View style={styles.wrapperItemSummary}>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Today</Text>
                                <Text style={styles.valueSummary}>60 Orang</Text>
                            </View>
                            <View style={styles.itemSummary}>
                                <Text style={styles.titleSummary}>Total</Text>
                                <Text style={styles.valueSummary}>100 Orang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.both} />
                </View>
            </ScrollView>
        </View>
    );

}

export default HomePage;

const styles = StyleSheet.create({
    containner: {
        height: height,
        width: width,
        backgroundColor: color.white,
        padding: width / 40
    },
    cardContainner: {
        width: '100%',
        marginBottom: 10
    },
    cardHeader: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: color.darkblue,
        borderTopStartRadius: 7,
        borderTopEndRadius: 7,
    },
    titleCard: {
        fontSize: 15,
        color: color.white
    },
    cardBody: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: color.blue,
        borderBottomStartRadius: 7,
        borderBottomEndRadius: 7,
        flexDirection: 'row',
    },
    itemKelas: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    iconItemKelas: {
        color: '#FFFFFF',
        fontSize: 55
    },
    imageItemKelas: {
        width: 60,
        height: 57
    },
    cardBodySummary: {
        backgroundColor: color.blue,
        borderBottomStartRadius: 7,
        borderBottomEndRadius: 7,
    },
    titleItemSammery: {
        color: color.white,
        fontWeight: 'bold',
        fontSize: 15,
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    wrapperItemSummary: {
        backgroundColor: color.darkblue,
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    itemSummary: {
        flexDirection: 'row',
        paddingVertical: 2,
        paddingHorizontal: 35
    },
    titleSummary: {
        flex: 1,
        textAlign: 'left',
        color: color.white
    },
    valueSummary: {
        flex: 1,
        textAlign: 'right',
        color: color.white
    },
    both: {
        height: 55
    }
});