import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
} from 'react-native';

const TugasPertama: () => React$Node = () => {
    return (
        <View style={styles.container}>
            <StatusBar />
            <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default TugasPertama;
